import 'package:blood/screens/StartScreen.dart';
import 'package:flutter/material.dart';

class AppBase extends StatelessWidget {
  const AppBase({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          canvasColor: const Color.fromARGB(255, 176, 39, 33),
        ),
        home: StartScreen(),
      );
}
