import 'package:blood/screens/SignInScreen.dart';
import 'package:blood/screens/requestScreen.dart';
import 'package:blood/widgets/Button.dart';
import 'package:blood/widgets/image.dart';
import 'package:flutter/material.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key key}) : super(key: key);

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
              ),
              DonateImage(),
              Text(
                '"Every blood donor is a life saver"',
                style: TextStyle(fontSize: 20),
              ),
              Builder(
                builder: (context) => Button(
                  text: "Become Donor",
                  width: 250,
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => SignIn()),
                    );
                  },
                ),
              ),
              Builder(
                builder: (context) => Button(
                  text: "Request for Blood",
                  width: 250,
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Request()),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
