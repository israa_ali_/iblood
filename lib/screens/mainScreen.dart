import 'package:blood/widgets/drawer.dart';
import 'package:blood/widgets/listOfDonors.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('home'),
          backgroundColor: const Color.fromARGB(255, 176, 39, 33),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications),
              onPressed: () {},
            )
          ],
        ),
        drawer: DrawerL(),
        body: UsersList(),
      ),
    );
  }
}
