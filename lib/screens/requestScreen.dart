import 'package:blood/screens/mainScreen.dart';
import 'package:blood/widgets/Button.dart';
import 'package:blood/widgets/list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Request extends StatefulWidget {
  Request({Key key}) : super(key: key);

  @override
  _RequestState createState() => _RequestState();
}

class _RequestState extends State<Request> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 176, 39, 33),
          title: Text('Request for Blood'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              'Select Blood Group',
              style: TextStyle(fontSize: 20),
            ),
            BloodGroup(),
            TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.phone),
                labelText: 'Contact number',
              ),
              keyboardType: TextInputType.phone,
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
              ],
            ),
            TextFormField(
              decoration: InputDecoration(
                icon: const Icon(Icons.location_on),
                labelText: 'Clinic',
              ),
              keyboardType: TextInputType.text,
            ),
            TextFormField(
              decoration: InputDecoration(
                icon: const Icon(Icons.person),
                labelText: 'Patient\'s condition',
              ),
              keyboardType: TextInputType.multiline,
              maxLines: 3,
            ),
            Builder(
              builder: (context) => Button(
                text: 'Send Request',
                width: 200,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => MainScreen()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
