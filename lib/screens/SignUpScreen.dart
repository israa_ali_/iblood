import 'package:blood/screens/mainScreen.dart';
import 'package:blood/widgets/Button.dart';
import 'package:blood/widgets/genderButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> _types = <String>[
    '',
    '+A',
    '-A',
    '+B',
    '-B',
    '+O',
    '-O',
    '-AB',
    '+AB'
  ];
  String _type = '';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: Text("Become Donor"),
            backgroundColor: const Color.fromARGB(255, 176, 39, 33),
          ),
          body: Center(
            child: Form(
              key: _formKey,
              autovalidate: true,
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 25.0),
                children: <Widget>[
                  TextFormField(
                    maxLines: 1,
                    decoration: InputDecoration(
                      icon: Icon(Icons.person),
                      fillColor: Colors.grey,
                      hintText: "Name",
                    ),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.phone),
                      labelText: 'Phone',
                    ),
                    keyboardType: TextInputType.phone,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.email),
                      labelText: 'Email',
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: const Icon(Icons.calendar_today),
                      labelText: 'Age',
                    ),
                    keyboardType: TextInputType.datetime,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: const Icon(Icons.location_on),
                      labelText: 'Address',
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,

                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Gender(),
                    ],
                  ),
                  new FormField(builder: (FormFieldState state) {
                    return InputDecorator(
                      decoration: InputDecoration(
                        icon: const Icon(Icons.opacity),
                        labelText: 'Blood Group',
                      ),
                      isEmpty: _type == '',
                      child: new DropdownButtonHideUnderline(
                        child: new DropdownButton(
                          value: _type,
                          isDense: true,
                          onChanged: (String newValue) {
                            setState(() {
                              //    newContact.favoriteColor = newValue;
                              _type = newValue;
                              state.didChange(newValue);
                            });
                          },
                          items: _types.map((String value) {
                            return new DropdownMenuItem(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    );
                  }),
                  SizedBox(
                    height: 40,
                  ),
                  Builder(
                      builder: (context) => Button(
                            text: 'Submit',
                            width: 100,
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => MainScreen()),
                              );
                            },
                          ))
                ],
              ),
            ),
          )),
    );
  }
}
