import 'package:blood/screens/StartScreen.dart';
import 'package:blood/screens/profileScreen.dart';
import 'package:flutter/material.dart';

class DrawerL extends StatelessWidget {
  const DrawerL({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final drawerheader = UserAccountsDrawerHeader(
      decoration: null,
      accountName: Text('user name'),
      accountEmail: Text('user.name@email.com'),
      currentAccountPicture: CircleAvatar(
        child: FlutterLogo(
          size: 42.0,
        ),
        backgroundColor: Colors.white,
      ),
    );

    final drawerItems = ListView(
      children: <Widget>[
        drawerheader,
        Builder(
          builder: (context) => ListTile(
            leading: Icon(Icons.person),
            title: Text(
              'profile',
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Profile()),
              );
            },
          ),
        ),
        Builder(
            builder: (context) => ListTile(
                  leading: Icon(Icons.arrow_back),
                  title: Text('Sign out'),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => StartScreen()),
                    );
                  },
                )),
      ],
    );

    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.5,
      child: Drawer(
        child: drawerItems,
      ),
    );
  }
}
