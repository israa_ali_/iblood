import 'package:flutter/material.dart';

class DonateImage extends StatelessWidget {
  const DonateImage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      width: 600,
      child: Image.asset('assets/Donate.png'),
    );
  }
}
