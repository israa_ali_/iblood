import 'package:flutter/material.dart';

class BloodGroup extends StatefulWidget {
  BloodGroup({Key key}) : super(key: key);

  @override
  _BloodGroup createState() => _BloodGroup();
}

class _BloodGroup extends State<BloodGroup> {
  List<String> _types = <String>[
    '',
    '+A',
    '-A',
    '+B',
    '-B',
    '+O',
    '-O',
    '-AB',
    '+AB'
  ];
  String _type = '';
  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: DropdownButton(
        value: _type,
        isDense: true,
        onChanged: (String newValue) {
          setState(() {
            //     newUser.bloodGroup = newValue;
            _type = newValue;
            //  state.didChange(newValue);
          });
        },
        items: _types.map((String value) {
          return new DropdownMenuItem(
            value: value,
            child: new Text(value),
          );
        }).toList(),
      ),
    );
  }
}
