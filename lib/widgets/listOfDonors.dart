import 'package:flutter/material.dart';

class UsersList extends StatelessWidget {
  const UsersList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const listTiles = <Widget>[
      ListTile(
        title: Text('UserName'),
        subtitle: Text('Contact Number : +9283745648'),
        trailing: Text(
          'A+',
          style: TextStyle(color: Colors.red, fontSize: 24),
        ),
        leading: Icon(Icons.person),
      ),
      ListTile(
        title: Text('User2'),
        subtitle: Text('Contact Number : +9283745648'),
        trailing: Text(
          'B+',
          style: TextStyle(color: Colors.red, fontSize: 24),
        ),
        leading: Icon(Icons.person),
      )
    ];
    return ListView(
      children: listTiles,
    );
  }
}
