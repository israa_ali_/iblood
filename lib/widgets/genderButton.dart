import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class Gender extends StatefulWidget {
  var type;

  @override
  _GenderState createState() => _GenderState();
}

class _GenderState extends State<Gender> {
  var _activeColor = const Color.fromARGB(255, 176, 39, 33);
  var labels = ['Female', 'Male'];
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RadioButtonGroup(
        orientation: GroupedButtonsOrientation.HORIZONTAL,
        margin: const EdgeInsets.only(left: 130.0),
        labels: labels,
        activeColor: _activeColor,
      ),
    );
  }
}
