import 'package:blood/appBase.dart';
import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(BaseApp());
}

class BaseApp extends StatelessWidget {
  const BaseApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBase();
  }
}
